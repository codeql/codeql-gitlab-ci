package metadata_test

import (
	"gitlab.com/codeql/codeql-gitlab/metadata"
	"reflect"
	"testing"

	report "gitlab.com/gitlab-org/security-products/analyzers/report/v3"
)

func TestReportScanner(t *testing.T) {
	want := report.ScannerDetails{
		ID:      "codeql",
		Name:    "CodeQL",
		Version: metadata.ScannerVersion,
		Vendor: report.Vendor{
			Name: "GitLab",
		},
		URL: "https://codeql.github.com",
	}
	got := metadata.ReportScanner

	if !reflect.DeepEqual(want, got) {
		t.Errorf("Wrong result. Expected:\n%#v\nbut got:\n%#v", want, got)
	}
}
