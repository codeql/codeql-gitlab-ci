FROM golang:1.18-alpine AS build
ENV CGO_ENABLED=0 GOOS=linux
WORKDIR /go/src/app
COPY . .
RUN go build -o analyzer

FROM registry.gitlab.com/codeql/codeql-base:2.12.6 AS codeql_base
RUN apt install maven -y
# Install analyzer
COPY --from=build /go/src/app/analyzer /analyzer
ENTRYPOINT [""]
