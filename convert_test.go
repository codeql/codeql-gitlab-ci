package main

import (
	metadata "gitlab.com/codeql/codeql-gitlab/metadata"
	"strings"
	"testing"

	"github.com/google/go-cmp/cmp"

	report "gitlab.com/gitlab-org/security-products/analyzers/report/v3"
	"gitlab.com/gitlab-org/security-products/analyzers/ruleset"
)

func TestConvert(t *testing.T) {
	in := ``

	var scanner = metadata.IssueScanner

	r := strings.NewReader(in)
	want := &report.Report{
		Version: report.CurrentVersion(),
		Vulnerabilities: []report.Vulnerability{
			{

				Category:   report.CategorySast,
				Message:    "Use of mark_safe() may expose cross-site scripting vulnerabilities and should be reviewed.",
				CompareKey: "app/tmp/py-app/cms/cms_plugins.py:3cec1ad7dbb0c4e3aadaa528a3ba3a998f558271046e748d45bd31f73ae7d602:B308",
				Severity:   report.SeverityLevelMedium,
				Confidence: report.ConfidenceLevelHigh,
				Scanner:    scanner,
				Location: report.Location{
					File:      "app/tmp/py-app/cms/cms_plugins.py",
					LineStart: 59,
					LineEnd:   59,
				},
				Identifiers: []report.Identifier{
					{
						Type:  "bandit_test_id",
						Name:  "Bandit Test ID B308",
						Value: "B308",
					},
				},
			},
			{
				Category:   report.CategorySast,
				Message:    "Use of assert detected. The enclosed code will be removed when compiling to optimised byte code.",
				CompareKey: "app/tmp/py-app/cms/models/pagemodel.py:5b8903fbc5b073ec09232c7536feac20906b8589f30278169d7b4f02108d8857:B101",
				Severity:   report.SeverityLevelLow,
				Confidence: report.ConfidenceLevelHigh,
				Scanner:    scanner,
				Location: report.Location{
					File:      "app/tmp/py-app/cms/models/pagemodel.py",
					LineStart: 831,
					LineEnd:   832,
				},
				Identifiers: []report.Identifier{
					{
						Type:  "bandit_test_id",
						Name:  "Bandit Test ID B101",
						Value: "B101",
						URL:   "https://bandit.readthedocs.io/en/latest/plugins/b101_assert_used.html",
					},
				},
			},
		},
		Analyzer:        "bandit",
		Config:          ruleset.Config{Path: ruleset.PathSAST},
		DependencyFiles: []report.DependencyFile{},
		Remediations:    nil,
	}
	got, err := convert(r, "app")
	if err != nil {
		t.Fatal(err)
	}
	if diff := cmp.Diff(want, got); diff != "" {
		t.Errorf("Report mismatch (-want +got):\n%s", diff)
	}
}
