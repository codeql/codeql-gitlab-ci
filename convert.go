package main

import (
	"bufio"
	"encoding/json"
	"fmt"
	"gitlab.com/codeql/codeql-gitlab/metadata"
	"gitlab.com/codeql/codeql-gitlab/sarif"
	"io"
	"io/ioutil"
	"strings"

	log "github.com/sirupsen/logrus"

	report "gitlab.com/gitlab-org/security-products/analyzers/report/v3"
	"gitlab.com/gitlab-org/security-products/analyzers/ruleset"
)

func convert(reader io.Reader, prependPath string) (*report.Report, error) {
	var reportPaths []string
	buffReader := bufio.NewReader(reader)
	line, _, err := buffReader.ReadLine()
	for {
		if err != nil {
			break
		}
		path := strings.TrimSpace(string(line))
		if path != "" {
			reportPaths = append(reportPaths, path)
		}
		line, _, err = buffReader.ReadLine()
	}
	var scanner = report.Scanner{
		ID:   metadata.AnalyzerID,
		Name: metadata.AnalyzerName,
	}
	var issues []report.Vulnerability
	for _, reportPath := range reportPaths {
		content, err := ioutil.ReadFile(reportPath)
		if err != nil {
			log.Error(err)
		} else {
			var sarifReport sarif.SarifReport
			_ = json.Unmarshal(content, &sarifReport)
			for _, run := range sarifReport.Runs {
				for _, result := range run.Results {
					rule := result.Rule(run.Rules())
					location := result.Location()
					if location != nil && rule != nil {
						var file = location.PhysicalLocation.ArtifactLocation.Uri
						var rLocation = report.Location{
							File:      file,
							Class:     file,
							LineStart: location.PhysicalLocation.Region.StartLine,
							LineEnd:   location.PhysicalLocation.Region.EndLine,
						}
						var compareKey = fmt.Sprintf("%s:%s:%s", rule.Id, rLocation.File, rLocation.LineStart)
						description := fmt.Sprintf("%s\n%s\n", rule.FullDescription.Text, result.Message.Text)
						flow := result.Flow()
						if flow != "" {
							description += fmt.Sprintf("##### Flow\n%s", flow)
						}
						issue := report.Vulnerability{
							Name:        fmt.Sprintf("[%s] %s @ %s", metadata.AnalyzerName, rule.ShortDescription.Text, file),
							Category:    report.CategorySast,
							Scanner:     scanner,
							Description: description,
							Location:    rLocation,
							Severity:    severityLevel(rule.Properties.Precision),
							CompareKey:  compareKey,
							Identifiers: rule.Identifiers(),
							Tracking: &report.Tracking{
								Type: "source",
								Items: []report.TrackingItem{
									{
										File:      file,
										LineStart: rLocation.LineStart,
										LineEnd:   rLocation.LineEnd,
										Signatures: []report.TrackingSignature{
											{
												Algorithm: "CodeQL-Algorithm",
												Value:     "NA",
											},
										},
									},
								},
							},
						}
						issues = append(issues, issue)
					}
				}
			}
		}
	}
	report := report.NewReport()
	report.Vulnerabilities = issues
	report.Analyzer = metadata.AnalyzerID
	report.Config.Path = ruleset.PathSAST
	return &report, nil
}

// severityLevel converts severity to a generic severity level.
func severityLevel(s string) report.SeverityLevel {
	s = strings.ToLower(s)
	switch s {
	case "critical":
		return report.SeverityLevelCritical
	case "very-high":
		return report.SeverityLevelCritical
	case "high":
		return report.SeverityLevelHigh
	case "medium":
		return report.SeverityLevelMedium
	case "low":
		return report.SeverityLevelLow
	case "info":
		return report.SeverityLevelInfo
	}
	return report.SeverityLevelUnknown
}
