package plugin

import (
	"gitlab.com/gitlab-org/security-products/analyzers/common/v2/plugin"
	"os"
)

func Match(path string, info os.FileInfo) (bool, error) {
	return true, nil
}

func init() {
	plugin.Register("codeql", Match)
}
