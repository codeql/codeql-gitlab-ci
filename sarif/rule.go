package sarif

import (
	"gitlab.com/gitlab-org/security-products/analyzers/report/v3"
	"regexp"
	"strings"
)

type Rule struct {
	Id               string `json:"id"`
	Name             string `json:"name"`
	ShortDescription struct {
		Text string `json:"text"`
	} `json:"shortDescription"`
	FullDescription struct {
		Text string `json:"text"`
	} `json:"fullDescription"`
	DefaultConfiguration struct {
		Enabled bool   `json:"enabled"`
		Level   string `json:"level,omitempty"`
	} `json:"defaultConfiguration"`
	Properties struct {
		Tags             []string `json:"tags"`
		Description      string   `json:"description"`
		Id               string   `json:"id"`
		Kind             string   `json:"kind"`
		Name             string   `json:"name"`
		Precision        string   `json:"precision,omitempty"`
		ProblemSeverity  string   `json:"problem.severity,omitempty"`
		SecuritySeverity string   `json:"security-severity,omitempty"`
	} `json:"properties"`
}

func (this *Rule) Identifiers() []report.Identifier {
	cweRegex, _ := regexp.Compile("CWE-[0-9]+")
	identifiers := []report.Identifier{}
	for _, tag := range this.Properties.Tags {
		tag = strings.ToUpper(tag)
		cwe := cweRegex.FindString(tag)
		if cwe != "" {
			i, success := report.ParseIdentifierID(cwe)
			if success == true {
				identifiers = append(identifiers, i)
			}
		}
	}
	return identifiers
}
