package sarif

type Run struct {
	Tool    Tool     `json:"tool"`
	Results []Result `json:"results,omitempty"`
}

func (this *Run) Rules() []Rule {
	return this.Tool.Driver.Rules
}

type Tool struct {
	Driver Driver `json:"driver"` // Tool Driver i.e Actual tool
}

type Driver struct {
	Name            string `json:"name"`
	Organization    string `json:"organization"`
	SemanticVersion string `json:"semanticVersion"`
	Rules           []Rule `json:"rules"`
}

type CodeFlow struct {
	ThreadFlows []ThreadFlow `json:"threadFlows"`
}

type ThreadFlow struct {
	Locations []Location `json:"locations"`
}

type PartialFingerprints struct {
	PrimaryLocationLineHash               string `json:"primaryLocationLineHash"`
	PrimaryLocationStartColumnFingerprint string `json:"primaryLocationStartColumnFingerprint"`
}
