package sarif

import (
	"fmt"
	"os"
	"strings"
)

type Result struct {
	RuleId    string `json:"ruleId"`
	RuleIndex int    `json:"ruleIndex"`
	Message   struct {
		Text string `json:"text"`
	} `json:"message"`
	Locations           []Location          `json:"locations"`
	PartialFingerprints PartialFingerprints `json:"partialFingerprints"`
	RelatedLocations    []RelatedLocations  `json:"relatedLocations,omitempty"`
	CodeFlows           []CodeFlow          `json:"codeFlows,omitempty"`
}

func (this *Result) Rule(rules []Rule) *Rule {
	for _, rule := range rules {
		if rule.Id == this.RuleId {
			return &rule
		}
	}
	return nil
}

func (this *Result) Location() *Location {
	for _, location := range this.Locations {
		if strings.HasPrefix(location.PhysicalLocation.ArtifactLocation.Uri, "file:/") == false {
			return &location
		}
	}
	return nil
}

func (this *Result) Flow() string {
	s := ""
	baseUri := fmt.Sprintf("%s/-/blob/%s", os.Getenv("CI_PROJECT_URL"), os.Getenv("CI_COMMIT_SHA"))
	for _, codeFlow := range this.CodeFlows {
		for _, threadFlow := range codeFlow.ThreadFlows {
			step := 1
			for _, location := range threadFlow.Locations {
				uri := fmt.Sprintf("%s/%s#L%s", baseUri, location.PhysicalLocation.ArtifactLocation.Uri, location.PhysicalLocation.Region.StartLine)
				s += fmt.Sprintf("%s. [[%s]](%s) %s:%s\n", step, location.Message.Text, uri, location.PhysicalLocation.ArtifactLocation.Uri, location.PhysicalLocation.Region.StartLine)
				step += 1
			}
			s += "_______________________"
		}
	}
	return s
}
