package sarif

type SarifReport struct {
	Version string `json:"version"`
	Schema  string `json:"$schema,omitempty"` // URI of SARIF Schema
	Runs    []Run  `json:"runs"`
}
