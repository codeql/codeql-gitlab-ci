package sarif

type Location struct {
	PhysicalLocation PhysicalLocation `json:"physicalLocation"`
	Message          struct {
		Text string `json:"text"`
	} `json:"message"`
}

type RelatedLocations struct {
	Id               int              `json:"id"`
	PhysicalLocation PhysicalLocation `json:"physicalLocation"`
	Message          struct {
		Text string `json:"text"`
	} `json:"message"`
}

type PhysicalLocation struct {
	ArtifactLocation ArtifactLocation `json:"artifactLocation"`
	Region           Region           `json:"region"`
}

type ArtifactLocation struct {
	Uri       string `json:"uri"`
	UriBaseId string `json:"uriBaseId"`
	Index     int    `json:"index"`
}

type Region struct {
	StartLine   int `json:"startLine"`
	StartColumn int `json:"startColumn"`
	EndColumn   int `json:"endColumn"`
	EndLine     int `json:"endLine,omitempty"`
}
