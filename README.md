# CodeQL analyzer for gitlab-ci

This analyzer is a wrapper around [CodeQL](https://codeql.github.com)
It's written in Go using
the [common library](https://gitlab.com/gitlab-org/security-products/analyzers/common)
shared by all analyzers.

The [common library](https://gitlab.com/gitlab-org/security-products/analyzers/common)
contains documentation on how to run, test and modify this analyzer.


## Usage
```yaml
updated
```

