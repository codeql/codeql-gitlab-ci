package main

import (
	"bufio"
	"errors"
	"fmt"
	log "github.com/sirupsen/logrus"
	"io"
	"io/ioutil"
	"os"
	"os/exec"
	"path/filepath"
	"strings"

	"github.com/urfave/cli/v2"
)

const (
	pathCodeql  = "codeql"
	resultsFile = "codeql_results.txt"
)

var languages = map[string]string{
	"c":          "c",
	"cpp":        "cpp",
	"csharp":     "csharp",
	"go":         "go",
	"java":       "java",
	"javascript": "javascript",
	"typescript": "javascript",
	"python":     "python",
	"ruby":       "ruby",
}

const (
	FlagLanguage   = "language"
	FlagCommand    = "command"
	FlagSourceRoot = "source-root"
	FlagOutput     = "output"
)

func analyzeFlags() []cli.Flag {
	return []cli.Flag{
		&cli.StringFlag{
			Name:    FlagLanguage,
			Aliases: []string{"l"},
			Usage:   "The language that the new database will be used to analyze",
			EnvVars: []string{"PROJECT_LANGUAGES", "CI_PROJECT_REPOSITORY_LANGUAGES"},
		},
		&cli.StringFlag{
			Name:    FlagCommand,
			Aliases: []string{"c"},
			Usage:   "For compiled languages, build commands that will cause the compiler to be invoked on thesource code to analyze",
			EnvVars: []string{"BUILD_COMMAND"},
		},
		&cli.StringFlag{
			Name:    FlagSourceRoot,
			Aliases: []string{"s"},
			Usage:   "[Default: .] The root source code directory. In many cases, this will be the checkout root. Files within it are considered to be the primary source files for this database. In some output formats, files will be referred to by their relative path from this directory.",
			Value:   ".",
			EnvVars: []string{"SOURCE_DIR"},
		},
		&cli.StringFlag{
			Name:    FlagOutput,
			Aliases: []string{"o"},
			Value:   "codeql-database",
			Usage:   "The directory of create database codeql",
		},
	}
}

func analyze(c *cli.Context, path string) (io.ReadCloser, error) {
	language := c.String(FlagLanguage)
	sourceDir := c.String(FlagSourceRoot)
	outputDir := c.String(FlagOutput)
	buildCommand := c.String(FlagCommand)
	languages := getProjectLanguages(language)
	_ = buildDatabase(languages, sourceDir, outputDir, buildCommand)
	var err error
	defer func() {
		if err != nil {
			log.Error("Analyze error: " + err.Error())
			os.Exit(2)
		} else {
			log.Info("Analyze success")
		}
	}()
	var results []string
	for _, language := range languages {
		db := outputDir
		if len(languages) > 1 {
			db = filepath.Join(db, language)
		}
		resultOutput := "codeql_" + language + ".sarif"
		// threads = 0 matches the number of threads to the number of logical processors
		cmd := exec.Command(pathCodeql, "database", "analyze", db, "--format", "sarif-latest", "--threads", "0", "--output", resultOutput)
		cmd.Env = os.Environ()
		log.Info("Analyze " + language)
		log.Info(cmd.String())
		stdout, _ := cmd.StdoutPipe()
		err = cmd.Start()
		if err != nil {
			return nil, err
		}
		go printStdout(stdout)
		err = cmd.Wait()
		if err == nil {
			results = append(results, resultOutput)
			log.Info("Analyze " + language + " success")
		} else {
			return nil, err
		}
	}
	_ = ioutil.WriteFile(resultsFile, []byte(strings.Join(results, "\n")), 0777)
	return os.Open(resultsFile)
}

func getProjectLanguages(language string) []string {
	var results []string
	for _, lang := range strings.Split(strings.TrimSpace(language), ",") {
		if languages[lang] != "" {
			results = append(results, languages[lang])
		}
	}
	return results
}

func buildDatabase(languages []string, sourceDir string, outputDir string, buildCommand string) error {
	var err error
	defer func() {
		if err != nil {
			log.Error("Build database error: " + err.Error())
			os.Exit(2)
		} else {
			log.Info("Build database success")
		}
	}()
	if len(languages) == 0 {
		err = errors.New("languages required")
		return err
	}
	options := []string{"database", "create", "--language", strings.Join(languages, ","), "--source-root", sourceDir, "--overwrite", outputDir}
	if buildCommand != "" {
		options = append(options, []string{"--command", buildCommand}...)
	}
	if len(languages) > 1 {
		options = append(options, []string{"--db-cluster", "--no-run-unnecessary-builds"}...)
	}
	cmd := exec.Command(pathCodeql, options...)
	cmd.Env = os.Environ()
	log.Info("Build database...")
	log.Info(cmd.String())
	stdout, _ := cmd.StdoutPipe()
	err = cmd.Start()
	if err != nil {
		return err
	}
	go printStdout(stdout)
	err = cmd.Wait()
	return err
}

func printStdout(stdout io.ReadCloser) {
	reader := bufio.NewReader(stdout)
	line, _, err := reader.ReadLine()
	for {
		if err != nil || line == nil {
			break
		}
		fmt.Println(string(line))
		line, _, err = reader.ReadLine()
	}
}
